import { Component, OnInit } from '@angular/core';
import { Person } from './shared/models/person.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  searchString='';
  title = 'project3';
  persons: Person[] = [];
  ngOnInit() {
    for (let i=0; i<10; i++){
      this.persons.push(new Person(i, 'Иван', 'Иванов', 9270740374));
    }
    
  }
  changePersons(person){
    this.searchString='';
    this.persons.splice(this.persons.findIndex(human => human.id == person.id), 1, person);
  }
  deletePersons(id){
    this.searchString='';
    this.persons.splice(this.persons.findIndex(human => human.id == id), 1);
  }
  addPersons(obj){
    let id = +this.persons[this.persons.length-1].id +1;
    this.searchString='';
    this.persons.push(new Person(id, obj.name_man, obj.surname_man, obj.phone_man ));
  }
    }


